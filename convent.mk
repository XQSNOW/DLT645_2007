# 将当前目录中的所有的.c .h .md文件的字符编码由GBK转换成为UTF8编码。
# 使用方法： make -f convent.mk 
# 在对字符编码进行转换的时候，会自动将源文件添加.back后缀。
# 如果不需要备份文件，可以使用 make -f convent clean清除。

# 注释需要顶格写。否则会被当做命令执行。
# 使用visual code编辑器时，默认的情况时将tab替换成空格。需要在设置中取消。

all:convent

THIS_PATH = ./
CFILES += $(wildcard $(THIS_PATH)*.c)  
HFILES += $(wildcard $(THIS_PATH)*.h)

ALL_FILE = $(CFILES) $(HFILES) $(DFILES)

convent:	
	for i in $(ALL_FILE); do cat $$i > $$i.backup ; done; #backup file
	rm $(ALL_FILE)
	for i in $(ALL_FILE); do iconv -f GBK -t UTF8 -c $$i.backup -o $$i ; done;
	
clean:
	rm *.backup
	
.PHONY: all clean convent 


